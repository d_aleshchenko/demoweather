//
//  ApplicationAssembly.swift
//  CitiesWeather
//
//  Created by Denis Aleschenko on 25.07.2022.
//

import UIKit

private enum Constants {
    enum Storyboards {
        static let citiesList = "CitiesList"
        static let detailedWeather = "DetailedWeather"
    }
    
}

protocol ApplicationAssemblyType {
    func listVC() -> UIViewController
    func detailsVC() -> UIViewController
}

class ApplicationAssembly: ApplicationAssemblyType {
    func listVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Constants.Storyboards.citiesList,
                                      bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CitiesListViewController") as! CitiesListViewController
        let interactor = CitiesListInteractor()
        let presenter = CitiesListPresenter()
        let router = CitiesListRouter()
        controller.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        interactor.citiesProvider = CitiesProviderService()
        presenter.viewController = controller
        router.viewController = controller
        router.dataStore = interactor

        return controller
    }
    
    func detailsVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Constants.Storyboards.detailedWeather, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DetailedWeatherViewController") as! DetailedWeatherViewController
        let interactor = DetailedWeatherInteractor()
        let presenter = DetailedWeatherPresenter()
        let router = DetailedWeatherRouter()
        controller.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.viewController = controller
        router.viewController = controller
        router.dataStore = interactor
        return controller
    }
}
