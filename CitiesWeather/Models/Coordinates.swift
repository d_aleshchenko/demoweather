//
//  Coordinates.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Foundation

struct Coordinates: Decodable {
    let lat: Double
    let lon: Double
}
