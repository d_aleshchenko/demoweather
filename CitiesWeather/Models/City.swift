//
//  City.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Foundation

struct City: Decodable {
    let id: Int
    let name: String
    let state: String
    let country: String
    let coord: Coordinates
}
