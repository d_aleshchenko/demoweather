//
//  CityTableViewCell.swift
//  CitiesWeather
//
//  Created by Denis Aleschenko on 20.07.2022.
//

import AlamofireImage
import UIKit

class CityTableViewCell: UITableViewCell {
    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var cityImageView: UIImageView!

    func setup(with viewModel: CitiesList.CityViewModel) {
        cityNameLabel.text = viewModel.name
        cityImageView.af.setImage(withURL: viewModel.imageUrl)
    }
}
