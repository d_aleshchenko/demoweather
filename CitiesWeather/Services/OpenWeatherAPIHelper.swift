//
//  OpenWeatherAPIHelper.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Foundation

struct OpenWeatherAPIHelper {
    private static let weatherRootURL = "https://api.openweathermap.org/data/2.5/weather?"
    private static let apiKey = "a35790b51cbba8e4cf2d8d5527aed671"

    static func getServerURL(coordinates: Coordinates) -> String {
        return "\(weatherRootURL)lat=\(coordinates.lat)&lon=\(coordinates.lon)&appid=\(OpenWeatherAPIHelper.apiKey)&units=metric"
    }
}
