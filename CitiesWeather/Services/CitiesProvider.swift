//
//  CitiesProvider.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Foundation

private enum Constants {
    static let citiesFileName = "city_list"
}

typealias Completion<T> = (Result<T, Error>) -> Void

enum CitiesProviderError: LocalizedError {
    case sourceNotFound
    case mappingFailed(String)

    var errorDescription: String? {
        switch self {
        case .sourceNotFound:
            return "Source file with raw Cities info does not found"
        case let .mappingFailed(message):
            return message
        }
    }
}

protocol CitiesProviderServiceType {
    func fetchCities(completion: @escaping Completion<[City]>)
}

class CitiesProviderService: CitiesProviderServiceType {
    func fetchCities(completion: @escaping Completion<[City]>) {
        guard let jsonData = JsonHelper.readFile(name: Constants.citiesFileName) else {
            completion(.failure(CitiesProviderError.sourceNotFound))
            return
        }
        let decoder = JSONDecoder()
        do {
            let cities = try decoder.decode([City].self, from: jsonData)
            completion(.success(cities))
        } catch {
            completion(.failure(CitiesProviderError.mappingFailed(error.localizedDescription)))
        }
    }
}
