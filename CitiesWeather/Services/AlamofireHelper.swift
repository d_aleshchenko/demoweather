//
//  AlamofireHelper.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Alamofire
import Foundation

struct AlamofireHelper {
    static let timeOut = 30.0

    static func createRequest(parameters: Parameters?, requestPath: String, httpMethod: HTTPMethod, contentType: String = "application/json") -> URLRequest? {
        guard let requestURL = URL(string: requestPath) else {
            return nil
        }
        var request = URLRequest(url: requestURL)
        request.httpMethod = httpMethod.rawValue
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = timeOut
        if let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        return request
    }
}
