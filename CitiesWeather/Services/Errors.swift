//
//  Errors.swift
//  CitiesApp (iOS)
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import Foundation

enum WeatherAppError: Error {
    case invalidLocationPermissions
    case invalidURLAPIRequest
    case invalidServerResponse
    case serverError
    case noInternetConnection
}
