//
//  DetailedWeatherInteractor.swift
//  CitiesApp
//
//  Created by Denis Aleschenko on 18.07.2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DetailedWeatherBusinessLogic {
    func doRequest(request: DetailedWeather.Request)
}

protocol DetailedWeatherDataStore {
    var cityCoordinates: Coordinates? { get set }
}

class DetailedWeatherInteractor: DetailedWeatherBusinessLogic, DetailedWeatherDataStore {
    var cityCoordinates: Coordinates?

    var presenter: DetailedWeatherPresentationLogic?
    var worker: DetailedWeatherWorker?

    // MARK: Do request

    func doRequest(request: DetailedWeather.Request) {
        worker = DetailedWeatherWorker()
        worker?.fetchWeatherForCurrentLocation(coordinates: request.coordinates, completion: { [weak self] result in
            switch result {
            case let .success(weather):
                self?.presenter?.present(weather: weather)
            case let .failure(error):
                self?.presenter?.present(errorDescription: error.localizedDescription)
            }
        })
    }
}
