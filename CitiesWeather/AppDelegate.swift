//
//  AppDelegate.swift
//  CitiesWeather
//
//  Created by Denis Aleschenko on 18.07.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let assembly = ApplicationAssembly()

        let navigation = UINavigationController(rootViewController: assembly.listVC())
        window?.rootViewController = navigation
        return true
    }
}
